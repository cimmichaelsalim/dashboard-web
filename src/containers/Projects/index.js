import React, { useState, useEffect, useMemo } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight, faChevronDown } from '@fortawesome/free-solid-svg-icons'
import { toast } from 'react-toastify'
import axios from 'axios'
import { parseISO, format, formatDistanceToNow } from 'date-fns'

import styles from './index.module.css'

const callClean = (projectName) => {
    axios.post(`http://localhost:4000/api/build/clean`, { project: projectName }).then(() => {
        toast.success("Successfully Clean project!")
    }).catch(e => {
        toast.error(`Failed. Message: ${e}`)
    })
}
const callBuild = (projectName, buildOption) => {
    axios.post(`http://localhost:4000/api/build/${buildOption}`, { project: projectName }).then(() => {
        toast.success("Successfully Built project!")
    }).catch(e => {
        toast.error(`Failed. Message: ${e}`)
    })
}

export default ({ toggleRefresh }) => {
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState(null)

    const [showStale, setShowStale] = useState(false)

    useEffect(() => {
        setData(null)
        setLoading(true)
        axios.get('http://localhost:4000/api/main').then(res => {
            setData(res.data)
            setLoading(false)
        })
    }, [toggleRefresh])

    if (loading) {
        return <div>Loading</div>
    }

    const dllTimes = Object.values(data).reduce((acc, val) => {
        return [...acc, ...val]
    }, []).reduce((acc, val) => ({ ...acc, [`${val.dllName}.dll`]: val.modifyTime }), {})


    return (
        <>
            {Object.entries(data).filter(([key, val]) => val.length > 0).map(([key, val]) => {
                return (
                    <Project key={`project_${key}`} title={key} data={val} dllTimes={dllTimes} />
                )
            })}
            <div className={styles.header}>
                <h3>Stale Projects</h3>
                <div className={styles.rightHeader}>
                    <p onClick={() => setShowStale(!showStale)}>
                        <FontAwesomeIcon icon={!showStale ? faChevronRight : faChevronDown} />
                    </p>
                </div>
            </div>
            {showStale && Object.entries(data).filter(([key, val]) => val.length === 0).map(([key]) => {
                return (
                    <StaleProject key={`stale_project_${key}`} title={key} />
                )
            })}
        </>
    )
}

const Project = ({ title, data, dllTimes }) => {
    const [open, setOpen] = useState(false)

    const modifiedData = useMemo(() => data.map(dll => {
        const pass = dll.dependencies.length === 0 || dll.dependencies.map(dependencyDll => {
            return parseISO(dll.modifyTime) > parseISO(dllTimes[dependencyDll])
        }).reduce((acc, val) => acc && val, true)

        return { ...dll, pass }
    }), [data, dllTimes])

    const allPass = useMemo(() => modifiedData.reduce((acc, val) => acc && val.pass, true), [modifiedData])

    useEffect(() => {
        if (!allPass) {
            setOpen(true)
        }
    }, [allPass])

    return (
        <div style={{ borderLeft: `10px solid ${allPass ? 'lightgreen' : 'salmon'}` }}>
            <div className={styles.lineContainer}>
                <p onClick={() => setOpen(!open)} className={styles.project}>
                    {title} <i>({data[0].branch})</i>
                </p>
                <div className={styles.buildContainer}>
                    <button onClick={() => { callClean(title) }}>
                        Clean
					</button>
                    <button onClick={() => { callBuild(title, 'release') }}>
                        Release
					</button>
                    <button onClick={() => { callBuild(title, 'debug') }}>
                        Debug
					</button>
                </div>
            </div>
            {open ?
                <div>
                    {modifiedData.map(x => (<div style={{ paddingLeft: 20, backgroundColor: x.pass ? "lightgreen" : 'salmon' }}><b>{x.dllName}</b> {format(parseISO(x.modifyTime), "dd LLL yyyy, HH:mm:ss")} - ({formatDistanceToNow(parseISO(x.modifyTime), { addSuffix: true })})</div>))}
                </div>
                : null
            }
        </div>
    )
}

const StaleProject = ({ title }) => {
    return (
        <div style={{ borderLeft: `10px solid grey` }}>
            <div className={styles.lineContainer}>
                <p className={styles.project} style={{ cursor: 'auto' }}>
                    {title}
                </p>
                <div className={styles.buildContainer}>
                    <button onClick={() => { callBuild(title, 'release') }}>
                        Release
          			</button>
                    <button onClick={() => { callBuild(title, 'debug') }}>
                        Debug
          			</button>
                </div>
            </div>
        </div>
    )
}