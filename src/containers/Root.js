import React, { useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify'
import axios from 'axios'

import Projects from './Projects'

import styles from './Root.module.css'

const openTSFolder = () => {
  axios.get(`http://localhost:4000/api/etc/open-traksys-folder`).then(() => {
    toast.success("Opening folder...")
  }).catch(e => {
    toast.error(`Failed. Message: ${e}`)
  })
}

const iisReset = () => {
  axios.get(`http://localhost:4000/api/etc/iisreset`).then(() => {
    toast.success("IIS reset successful")
  }).catch(e => {
    toast.error(`Failed. Message: ${e}`)
  })
}

export default () => {
  const [toggleRefresh, setToggleRefresh] = useState(false)
  const [reloadPeriod, setReloadPeriod] = useState(60000)

  useEffect(() => {
    const interval = setInterval(() => { setToggleRefresh(t => !t) }, reloadPeriod)
    return () => clearInterval(interval)
  }, [reloadPeriod])

  return (
    <div className="App">
      <div className={styles.header}>
        <h3>Projects</h3>
        <div className={styles.rightHeader}>
          <p onClick={() => openTSFolder()} style={{ marginRight: 15 }}>Open TS Folder</p>
          <p onClick={() => iisReset()}>IIS Reset</p>
          <input type="text" value={reloadPeriod} onChange={(e) => setReloadPeriod(e.target.value)} />
          <p onClick={() => setToggleRefresh(!toggleRefresh)}>Refresh</p>
        </div>
      </div>

      <Projects toggleRefresh={toggleRefresh} />

      <ToastContainer
        position="bottom-left"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnVisibilityChange
        draggable
        pauseOnHover
      />
    </div>
  );
}